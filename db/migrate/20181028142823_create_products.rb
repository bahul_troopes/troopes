class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.string :permalinkr
      t.string :meta_description
      t.string :meta_keywords
      t.timestamps
    end
  end
end
